﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Actor
{
    public float moveSpeed = 5.0f;
    public Rigidbody2D playerRigidBody;
    public Animator playerAnimator;

    private Vector2 _movement;

    public void Start()
    {
        if(GameState.LastPlayerPosition.HasValue)
        {
            transform.position = GameState.LastPlayerPosition.Value;
        }
    }

    private bool IsAbleToActionInput()
    {
        if (GameState.InputSuspended)
        {
            return false;
        }

        return true;
    }

    public void FixedUpdate()
    {
        if (IsAbleToActionInput())
        {
            playerRigidBody.MovePosition(playerRigidBody.position + _movement * moveSpeed * Time.fixedDeltaTime);
        }
    }

    public override void OnFullPrimaryAxisInput(float horizontalAxisInput, float verticalAxisInput)
    {
        if (!IsAbleToActionInput())
        {
            return;
        }

        MovePlayer(horizontalAxisInput, verticalAxisInput);
    }

    public override void OnFullPrimaryAxisInputHeld(float horizontalAxisInput, float verticalAxisInput)
    {
        if (!IsAbleToActionInput())
        {
            return;
        }

        MovePlayer(horizontalAxisInput, verticalAxisInput);
    }

    private void MovePlayer(float horizontalAxisInput, float verticalAxisInput)
    {
        if (IsAbleToActionInput())
        {
            _movement.x = horizontalAxisInput;
            _movement.y = verticalAxisInput;

            playerAnimator.SetFloat("Horizontal", _movement.x);
            playerAnimator.SetFloat("Vertical", _movement.y);
            playerAnimator.SetFloat("Speed", _movement.sqrMagnitude);
        }
        else
        {
            //A little inefficient, but this is what has her turn back to the screen and stop
            playerAnimator.SetFloat("Speed", 0);
        }
    }
}
