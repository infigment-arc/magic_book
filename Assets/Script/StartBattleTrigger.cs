﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartBattleTrigger : MonoBehaviour
{
    public string SceneName = "BattleScene";

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Battle trigger has been invoked");
        GameState.LastPlayerPosition = collision.attachedRigidbody.position;
        TriggerBattleTransition();
    }

    private void TriggerBattleTransition()
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        BattleTransition battleTransition = camera.GetComponent<BattleTransition>();
        battleTransition.StartTransitionIn(LoadBattleScene);
    }

    private void LoadBattleScene()
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }
}
