﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface InterfaceItem
{
    /// <summary>
    /// Generic "use" terminology.
    /// Food / Drink / Potions: Consume
    /// Weapons / Armour: Equipt
    /// Book: Read
    /// </summary>
    void Use();

    /// <summary>
    /// Different items might trigger different sounds when dropped
    /// Bottle smash / weapons clang / etc
    /// </summary>
    void Drop();

    /// <summary>
    /// Examine the item to get some information about the usage.
    /// e.g; "Small health potion: Heals party member 30hp"
    /// </summary>
    void Examine();
}
