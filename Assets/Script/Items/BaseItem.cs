﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseItem : MonoBehaviour, InterfaceItem
{
    /// <summary>
    /// Description of the item.
    /// </summary>
    public string description;

    /// <summary>
    /// Icon shown in inventory.
    /// </summary>
    public Sprite icon;

    public void Drop()
    {
        Destroy(gameObject);
    }

    public void Examine()
    {
        Debug.Log(description);
    }

    public abstract void Use();
}
