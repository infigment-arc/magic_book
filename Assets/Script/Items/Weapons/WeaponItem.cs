﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItem : BaseItem
{
    public float attackSpeed;
    public float AttackSpeed
    {
        get
        {
            return attackSpeed;
        }

        set
        {
            attackSpeed = value;
        }
    }

    public float attackDamage;
    public float AttackDamage
    {
        get
        {
            return attackDamage;
        }

        set
        {
            attackDamage = value;
        }
    }

    public override void Use()
    {
        //If we have the weapon equipt then remove it
        //If we don't have the weapon equipt then equipt it
        throw new System.NotImplementedException();
    }
}
