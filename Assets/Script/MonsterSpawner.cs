﻿using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public GameObject monster;
    public float spawnRadius;

    void Awake()
    {
        Vector3 spawnPoint = SelectSpawnPoint();
        if(GameState.LastPlayerPosition.HasValue)
        {
            Debug.Log("Dist: " + Vector3.Distance(spawnPoint, GameState.LastPlayerPosition.Value));
            while (Vector3.Distance(spawnPoint, GameState.LastPlayerPosition.Value) < 6f)
            {
                Debug.Log("Dist: " + Vector3.Distance(spawnPoint, GameState.LastPlayerPosition.Value));
                spawnPoint = SelectSpawnPoint();
            }
        }

        Instantiate(monster, spawnPoint, Quaternion.identity);
    }

    private Vector3 SelectSpawnPoint()
    {
        Vector3 spawnPoint = transform.position;

        spawnPoint.x += Random.Range(-spawnRadius, spawnRadius);
        spawnPoint.y += Random.Range(-spawnRadius, spawnRadius);

        return spawnPoint;
    }
}
