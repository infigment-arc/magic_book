﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndBattleBehaviour : MonoBehaviour
{
    public void EndBattle()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
