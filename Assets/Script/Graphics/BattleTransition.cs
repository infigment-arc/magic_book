﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTransition : MonoBehaviour
{
    public Material TransitionMaterial;
    private bool TransitionIn { get; set; } = false;
    private float TransitionProgress { get; set; } = 0;
    public float transitionSpeed = 0.01f;
    private Action TransitionCallback { get; set; }

    public void Start()
    {
        ResetTransition();
    }

    private void ResetTransition()
    {
        TransitionProgress = 0;
        TransitionMaterial.SetFloat("_Cutoff", TransitionProgress);
        TransitionCallback = null;
        TransitionIn = false;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, TransitionMaterial);
    }

    public void StartTransitionIn(Action transitionEndCallback)
    {
        if(!TransitionIn)
        {
            TransitionIn = true;
            TransitionCallback = transitionEndCallback;
            GameState.InputSuspended = true;
        }
    }

    public void Update()
    {
        if(TransitionIn && TransitionProgress < 1)
        {
            TransitionProgress += transitionSpeed;
            TransitionMaterial.SetFloat("_Cutoff", TransitionProgress);

            if(TransitionProgress >= 1)
            {
                TransitionCallback?.Invoke();
                GameState.InputSuspended = false;
            }
        }
    }
}
