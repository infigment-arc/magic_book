﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Test"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _TransitionTex ("Transition Texture", 2D) = "white" {}
		_Color ("Screen Color", Color) = (1,1,1,1)
		_Cutoff ("Cutoff", Range(0,1)) = 0
		_Fade ("Fade", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				o.uv1 = v.uv;

				//#if UNITY_UV_STARTS_AT_TOP
				//if (_MainTex_TexelSize.y < 0)
				//	o.uv1.y = 1 - o.uv1.y;
				//#endif

                return o;
            }
            
			sampler2D _MainTex;
			sampler2D _TransitionTex;
			float _Cutoff;
			fixed4 _Color;
			float _Fade;
			fixed4 col;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 transit = tex2D(_TransitionTex, i.uv);
				if (transit.b < _Cutoff)
					return col = lerp(col, _Color, _Fade);

				return tex2D(_MainTex, i.uv);
            }
            ENDCG
        }
    }
}
