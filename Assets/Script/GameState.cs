﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameState
{
    public static Vector2? LastPlayerPosition { get; set; } = null;
    public static bool InputSuspended { get; set; } = false;
    public static string TargetPortal { get; set; } = null;
}
