﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickupTrigger : MonoBehaviour
{
    //! The prefab that will enter the player's inventory. This object should contain all the information
    //! required in order to use the item - to be expanded later.
    public BaseItem item;

    private GameObject collisionObject = null;

    public void LateUpdate()
    {
        if (collisionObject != null)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                //To start with we'll just pick up the item and send it to the player's inventory
                Inventory inventory = collisionObject.GetComponent<Inventory>();
                if (inventory != null)
                {
                    //Add the item to the inventory
                    bool success = inventory.AddItem(item);
                    if(success)
                    {
                        //Item has been picked up, remove the trigger object.
                        Destroy(gameObject);
                    }
                }
            }
            else if(Input.GetKeyDown(KeyCode.E))
            {
                //! Examine the item on the ground.
                item.Examine();
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Debug.Log("Press space to pickup or 'e' to examine!");

            //Store the collision item so we can perform the action in the update.
            collisionObject = collision.gameObject;
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        //! Initially attempted to use this for determining when enter was hit
        //! but this function is not hit constantly. Instead using Enter/Exit/Update
        //! functionality in order to register pickups.
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            //Clear the collision item so we don't perform the action in the update.
            collisionObject = null;
        }
    }
}
