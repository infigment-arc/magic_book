﻿public enum ItemType
{
	NONE,
	SEED,
	PRODUCE,
	TOOL
};
