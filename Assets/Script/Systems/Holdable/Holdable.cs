﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Holdable
{
	string GetName();
	ItemType GetItemType();
	void OnHeld();
	void OnReleased();
}
