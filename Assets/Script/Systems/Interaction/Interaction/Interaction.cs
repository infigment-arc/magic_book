﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* The Interaction is one of the core aspects of the interaction system.
 * An action that extends it will need to implement Execute, which should set
 * into motion the interaction. This will be called from the script which
 * requests the interactions.
 * 
 * In order to be eligible to be returned, an interaction lists the
 * requirements of the Agent and Interactable's states that it would
 * need in order to run.
 * 
 */

public abstract class Interaction : MonoBehaviour
{
	public enum interactionCategory
	{
		PROGRESS,
		REGRESS
	};

	public enum stateSourceType
	{
		NONE,
		AGENT,
		INTERACTABLE
	};

	public enum stateType
	{
		NONE,
		INVENTORY,
		INVENTORY_IS_EMPTY,
		INVENTORY_HAS_SPACE,
		HAS_ENOUGH_CASH,
	};

	public struct Requirement
	{
		private stateSourceType sourceType;
		private stateType stateValueType;
		private string stateValue;

		public stateSourceType SourceType
		{
			get;
			set;
		}

		public stateType StateValueType
		{
			get;
			set;
		}

		public string StateValue
		{
			get;
			set;
		}

		public Requirement(stateSourceType source, stateType type, string val) : this()
		{
			SourceType = source;
			StateValueType = type;
			StateValue = val;
		}
	};

	private String interactionName;
	private List<Requirement> requirements;
	private interactionCategory category;
    private bool isActive;

	public interactionCategory Category { get; set; }

    public bool IsActive
    {
        get
        {
            return isActive;
        }

        set
        {
            isActive = value;
        }
    }

    public abstract void Execute(Agent target);

	public Interaction()
	{
		requirements = new List<Requirement>();
		Category = interactionCategory.PROGRESS;
        IsActive = true;
	}

	public void SetName(String name)
	{
		this.interactionName = name;
	}

	public String GetName()
	{
		return interactionName;
	}

	public void AddRequirement(Requirement requirement)
	{
		requirements.Add(requirement);
	}

	public void ClearRequirements()
	{
		requirements.Clear();
	}

	public virtual bool DoObjectStatesCoverRequirements(Agent targetAgent, Interactable source)
	{
        if(!IsActive)
        {
            return false;
        }

		foreach (Requirement requirement in requirements)
		{
			if (requirement.SourceType == stateSourceType.AGENT)
			{
				if (!DoesAgentCoverRequirement(requirement, targetAgent))
				{
					Debug.Log("Agent does not pass " + GetName() + " requirements");
					return false;
				}
			}
			else if (requirement.SourceType == stateSourceType.INTERACTABLE)
			{
				if (!DoesInteractableCoverRequirement(requirement, source))
				{
					Debug.Log("Interactable does not pass " + GetName() + " interaction requirements.");
					return false;
				}
			}
		}
		Debug.Log("Object states have passed " + GetName() + " interaction requirements!");
		return true;
	}

	protected bool DoesAgentCoverRequirement(Requirement requirement, Agent agent)
	{
		if (requirement.StateValueType == stateType.INVENTORY)
		{
			Debug.Log("Checking for item with name " + requirement.StateValue + " to cover requierments for " + GetName());
			GameObject item = agent.GetInventory().FindItemOfType(requirement.StateValue);
			return (item != null);
		}
		else if (requirement.StateValueType == stateType.INVENTORY_IS_EMPTY)
		{
			return agent.GetInventory().IsEmpty();
		}
		else if (requirement.StateValueType == stateType.INVENTORY_HAS_SPACE)
		{
			return !agent.GetInventory().IsFull();
		}
		else if(requirement.StateValueType == stateType.HAS_ENOUGH_CASH)
		{
			int amount;
			if(!int.TryParse(requirement.StateValue, out amount))
			{
				return false;
			}
			Debug.Log("Checking that character has enough cash: " + amount + " to cover requirements for " + GetName());
			//return agent.GetInventory().HasCash(amount);
			return false;
		}

		return true;
	}

	protected bool DoesInteractableCoverRequirement(Requirement requirement, Interactable interactable)
	{
		//we can have the interactable define a state struct later on if we need it
		return true;
	}

}

