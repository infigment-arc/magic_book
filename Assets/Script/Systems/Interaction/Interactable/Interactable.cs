﻿using System.Collections;
using System.Collections.Generic;

/* Interactable is the main, simple, interface which the
 * Interaction system relies on. Anything that is interactable
 * will implement this and pass a list of Interactions based on
 * the state of the agent passed in. 
 * 
 * The agent can then execute one of those interactions.
 * 
 */

//We should move this
public enum stateProperties
{
	IS_WATERABLE,
	CAN_DEAL_DAMAGE,
	IS_TARGETED,
	IS_RAKEABLE
};

public interface Interactable
{
	List<Interaction> RequestInteractions(Agent agent, Interaction.interactionCategory interactionCategory);
	bool HasProperty(stateProperties toCheck);
}
