﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableStateHolder : StateHolder, Interactable
{
	public InteractableStateHolder()
	{

	}

	public virtual List<Interaction> RequestInteractions(Agent agent, Interaction.interactionCategory interactionCategory) { return new List<Interaction>(); }

	public virtual bool HasProperty(stateProperties toCheck)
	{
		return StatePropertyExists(toCheck);
	}
}
