﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AgentStateHolder : StateHolder, Agent
{
	public AgentStateHolder()
	{

	}

	public bool AddItem(GameObject item) { return false; }
	public void RemoveItem(GameObject item) { }
	public Inventory GetInventory() { return new Inventory(); }
	public GameObject GetTarget() { return new GameObject(); }
	public GameObject GetGameObject() { return gameObject; }

	public virtual bool HasProperty(stateProperties toCheck)
	{
		return StatePropertyExists(toCheck);
	}
}
