﻿using System;
using UnityEngine;

/* Agent is part of the interaction system.
 * <FILL IN LATER>
 * 
 */

public interface Agent
{
	//get State()
	bool AddItem(GameObject item);
	void RemoveItem(GameObject item);
	Inventory GetInventory();
	GameObject GetTarget();

	GameObject GetGameObject();

	bool HasProperty(stateProperties toCheck);
}

