﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateHolder : MonoBehaviour
{
	private List<stateProperties> currentStateProperties;

	public StateHolder()
	{
		currentStateProperties = new List<stateProperties>();
	}

	public void AddStateProperty(stateProperties toAdd)
	{
		currentStateProperties.Add(toAdd);
	}

	public void RemoveStateProperty(stateProperties toRemove)
	{
		currentStateProperties.Remove(toRemove);
	}

	public void ClearStateProperties()
	{
		currentStateProperties.Clear();
	}

	public bool StatePropertyExists(stateProperties toCheck)
	{
		return getCurrentStateProperties().Contains(toCheck);
	}

	public List<stateProperties> getCurrentStateProperties()
	{
		return currentStateProperties;
	}
}
