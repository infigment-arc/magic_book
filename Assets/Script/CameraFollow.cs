﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aim is to just follow the player for now. Could give the camera bounds in the future.
public class CameraFollow : MonoBehaviour
{
    public GameObject CameraTarget;

    private Vector3 LastTargetPosition { get; set; }
    private Vector3 CurrentTargetPosition { get; set; }

    private const float CameraSpeed = 0.2f;
    private float currentLerpProgress = 0.0f;

    void Start()
    {
        Vector3 playerPostion = CameraTarget.transform.position;
        Vector3 cameraPosition = transform.position;

        Vector3 startTargetPosition = playerPostion;
        startTargetPosition.z = cameraPosition.z;

        CurrentTargetPosition = startTargetPosition;
        LastTargetPosition = startTargetPosition;
    }

    public void FixedUpdate()
    {
        TrackCameraTarget();
        if(!ReachedTarget())
        {
            MoveCamera();
        }
    }

    private bool ReachedTarget()
    {
        Vector3 currentCameraPosition = transform.position;

        return CurrentTargetPosition.x == currentCameraPosition.x &&
               CurrentTargetPosition.y == currentCameraPosition.y;
    }

    private void TrackCameraTarget()
    {
        Vector3 targetPosition = CameraTarget.transform.position;
        targetPosition.z = transform.position.z;

        LastTargetPosition = CurrentTargetPosition;
        CurrentTargetPosition = targetPosition;

        currentLerpProgress = 0.0f;
    }

    private void MoveCamera()
    {
        currentLerpProgress += CameraSpeed;
        transform.position = Vector3.Lerp(LastTargetPosition, CurrentTargetPosition, currentLerpProgress);
    }
}
