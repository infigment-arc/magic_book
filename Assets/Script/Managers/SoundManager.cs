using UnityEngine;
using System;
using System.Collections.Generic;
using SoundDefs;

public class SoundManager : MonoBehaviour
{
	[SerializeField]
	private SoundIdSoundDescriptorDictionary soundBankStore = SoundIdSoundDescriptorDictionary.New<SoundIdSoundDescriptorDictionary>();
	private Dictionary<SoundId, SoundDescriptor> soundBank 
	{
	    get { return soundBankStore.dictionary; }
	}

	[Range(0.0f, 1.0f)]
	public float volume;

	public void PlayAudio(SoundId soundId, AudioSource source)
	{
		Debug.Log("Received request to play sound with id: " + soundId);
		if(!soundBank.ContainsKey(soundId))
		{
			Debug.Log("Could not play sound with Id: " + soundId);
			return;
		}

		source.clip = soundBank[soundId].soundClip;
		if(soundBank[soundId].repeat)
		{
			source.loop = true;
			source.Play();
		}
		else
		{
			source.loop = false;
			source.PlayOneShot(source.clip, volume);
		}
	}
}
