﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    [SerializeField]
    private StringSpriteDictionary produceSpriteStore = StringSpriteDictionary.New<StringSpriteDictionary>();
    private Dictionary<string, Sprite> produceSpriteMap
    {
        get { return produceSpriteStore.dictionary; }
    }

    public Sprite GetSpriteForProduce(string produceName)
    {
        return produceSpriteMap[produceName];
    }

}
