﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    public string Name;
    public string TargetScene;
    public string TargetPortalName;

    public void Awake()
    {
        if(GameState.TargetPortal == Name)
        {
            GameState.TargetPortal = null;
            SetCharacterPosition(transform.position);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Portal trigger has been invoked");
        if(string.IsNullOrWhiteSpace(TargetScene) || TargetScene == SceneManager.GetActiveScene().name)
        {
            GameObject target = FindPortalByName(TargetPortalName);
            SetCharacterPosition(target.transform.position);
            return;
        }

        GameState.TargetPortal = TargetPortalName;
        LoadTargetScene();
    }

    private void SetCharacterPosition(Vector3 position)
    {
        GameObject character = GameObject.FindGameObjectWithTag("Player");
        character.transform.position = new Vector3(position.x, position.y, character.transform.position.z);
    }

    private GameObject FindPortalByName(string targetPortalName)
    {
        Debug.Log($"Target Portal name is {targetPortalName}");
        List<GameObject> portals = GameObject.FindGameObjectsWithTag("Portal").ToList();
        return portals.FirstOrDefault(portal => portal.GetComponent<Portal>().Name == targetPortalName);
    }

    private void LoadTargetScene()
    {
        SceneManager.LoadScene(TargetScene, LoadSceneMode.Single);
    }
}
