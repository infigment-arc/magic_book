﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	private List<BaseItem> items = null;
	public int maximumItems = 10;

	private void Awake()
	{
		items = new List<BaseItem>();
	}

	public bool AddItem(BaseItem newItem)
	{
		if(items.Count == maximumItems)
		{
			Debug.Log("Inventory is already full! Please drop some items.");
			return false;
		}

		items.Add(newItem);
		return true;
	}

	public GameObject FindItemOfType(string type)
	{
		return null;
	}

	public bool IsFull()
	{
		return items != null;
	}

	public bool IsEmpty()
	{
		return items == null;
	}
}
